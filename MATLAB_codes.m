%%%%%%%%%%%%%%%%%%%%%%%%%% Autoencoder-5fold %%%%%%%%%%%%%%%%%%%%%%%%%%
%% We need to repeat this for different occ algorithms with hdifferent parameteres different values for k in kfold cross validation
clc
clear
close

% The path to MATLAB OCC library is placed here
oneClassLibrary = genpath('');
% The path to data is placed here
dataPath = genpath('');
addpath(oneClassLibrary);
addpath(dataPath);

max_fold = 5;

load krl
load knrl
% load krl
% load knrl

X = [krl;knrl];
Y = ones(size(X,1),1);
Y(size(krl,1)+1:end) = 2;

% X = [krl;knrl];
% Y = ones(size(X,1),1);
% Y(size(krl,1)+1:end) = 2;

indicies = crossvalind('Kfold',Y,max_fold);

i=0;
j=0;
for pr1 = 0.1:0.1:1
    pr1
    i = i + 1;
    for pr2 = 1:1:5
        tic;
        pr2
        j = j + 1;
        
        TN = []; FP = []; FN = []; TP = [];
        for fold=1:1:max_fold
            Xtr = X(indicies ~= fold,:);
            Ytr = Y(indicies ~= fold);
            Xts = X(indicies == fold,:);
            Yts = Y(indicies == fold);
            
            tr = gendatoc(Xtr(Ytr==1,:),Xtr(Ytr~=1,:));
            tr = oc_set(tr,1);
            ts = gendatoc(Xts(Yts==1,:),Xts(Yts~=1,:));
            ts = oc_set(ts,1);
           
            autoencoder_classifier = autoenc_dd(tr,pr1,pr2);
            
            [C,LABLIST] = dd_confmat(ts, autoencoder_classifier);

            TN = [TN, C(1,1)];
            FP = [FP, C(1,2)];
            FN = [FN, C(2,1)];
            TP = [TP, C(2,2)]; 
            
        end
        
        tn = sum(TN)/ max_fold;
        fp = sum(FP)/ max_fold;
        fn = sum(FN)/ max_fold;
        tp = sum(TP)/ max_fold;
        
        pr = tp / (tp+fp);
        re = tp / (tp+fn);
        f1 = (2*pr*re)/(pr+re);
        ac = (tp+tn)/(tp+tn+fp+fn);
        
        autoencoder_p1(i,j) = pr1;
        autoencoder_p2(i,j) = pr2;
        autoencoder_tp(i,j) = tp;
        autoencoder_fp(i,j) = fp;
        autoencoder_tn(i,j) = tn;
        autoencoder_fn(i,j) = fn;
        autoencoder_pr(i,j) = pr;
        autoencoder_re(i,j) = re;
        autoencoder_ac(i,j) = ac;
        autoencoder_f1(i,j) = f1;
        
        interval = toc;
        autoencoder_interval(i,j) = interval/max_fold;
    end
    j = 0;
end

csvwrite('autoencoder_p1_5fold_krl.csv', autoencoder_p1) 
csvwrite('autoencoder_p2_5fold_krl.csv', autoencoder_p2) 
csvwrite('autoencoder_tp_5fold_krl.csv', autoencoder_tp) 
csvwrite('autoencoder_fp_5fold_krl.csv', autoencoder_fp) 
csvwrite('autoencoder_tn_5fold_krl.csv', autoencoder_tn) 
csvwrite('autoencoder_fn_5fold_krl.csv', autoencoder_fn) 
csvwrite('autoencoder_pr_5fold_krl.csv', autoencoder_pr) 
csvwrite('autoencoder_re_5fold_krl.csv', autoencoder_re) 
csvwrite('autoencoder_ac_5fold_krl.csv', autoencoder_ac) 
csvwrite('autoencoder_f1_5fold_krl.csv', autoencoder_f1) 
csvwrite('autoencoder_interval_5fold_krl.csv', autoencoder_interval)

save('autoencoder_5fold_krl.mat')