# IEEE TCSS Paper

This project consists of the codes used in "Computational Rumor Detection Without Non-Rumor: A One-Class Classification Approach" paper, published in IEEE Transactions on Computational Social Systems.
